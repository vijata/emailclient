import React, { Component } from 'react';
import { Collapse, List, Row, Col } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import "../css/style.css"

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}

class MyComponent extends Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: [],
        mails: [],
        mailList: false,
        mailView: false,
        mailDetail: [],
        
      };
    }

    componentDidMount() {
      fetch("http://localhost:8082/folders", {
        headers: {
            'Authorization': "Bearer hfjhfg2h3jf423f4hj3f24g234v23jhf4j23fbu4jf23j423jf4hj23b4jnfdsf8td7s8fnt8dsfmsdf",
            // 'X-FP-API-KEY': 'iphone', //it can be iPhone or your any other attribute
            // 'Content-Type': 'application/json'
        }

      })
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              items: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
    
    handleMailList = (folderID) => {
        fetch("http://localhost:8082/folders/"+folderID+"/emails", {
          headers: {
              'Authorization': "Bearer hfjhfg2h3jf423f4hj3f24g234v23jhf4j23fbu4jf23j423jf4hj23b4jnfdsf8td7s8fnt8dsfmsdf",
              // 'X-FP-API-KEY': 'iphone', //it can be iPhone or your any other attribute
              // 'Content-Type': 'application/json'
          }
  
        })
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                mails: result,
                mailList: true,
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    }

    handleMailView = (mail) => {
        this.setState({
          mailView: true,
          mailDetail: mail,
        });
    }

    handleDelete = (mailID) => {
        console.log(mailID);
        fetch("http://localhost:8082/emails/"+mailID.id, {
          headers: {
              'Authorization': "Bearer hfjhfg2h3jf423f4hj3f24g234v23jhf4j23fbu4jf23j423jf4hj23b4jnfdsf8td7s8fnt8dsfmsdf",
              // 'X-FP-API-KEY': 'iphone', //it can be iPhone or your any other attribute
              // 'Content-Type': 'application/json'
          }
        })
          .then(res => res.json())
          .then(
            (result) => {
                const newMailList = [...this.state.mails];
                const index = newMailList.indexOf(mailID);
                const mails = newMailList.filter((m) => !(m.id === mailID.id) )

                this.setState({
                    mails: mails
                });
              console.log(this.state.mails)
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    }

    render() {
        const { items, mails, mailList, mailDetail, mailView } = this.state;
        
        return (
            <div>
            <Row>
                <Col span={8}>
                    <Collapse defaultActiveKey={['1']} onChange={callback} className="folders">
                            <Panel header="Folder">
                                {items.map(item => (
                                    <List key={item.titile} onClick={() => this.handleMailList(item.id)}> {item.titile}</List>
                                ))}
                                
                            </Panel>
                    </Collapse>
                </Col>
                <Col span={8}>
                    {mailList ? 
                    <div className="mail-list">
                        {/* <h1>{mails.folderId}</h1> */}
                        {mails.map(item => (
                            <List key={item.id} onClick={() => this.handleMailView(item)}>
                                {item.title}
                                <DeleteOutlined onClick={() => this.handleDelete(item)}/>
                            </List>
                        ))}
                    </div>
                    : null
                    }
                            
                </Col>
                <Col span={8}>
                    {mailView ?
                    <div className="mailPreview">
                        <h3 className="mail-title"><label>Title: </label>{mailDetail.title}</h3>
                        <h4 className="mail-subject"><label>Subject: </label>{mailDetail.subject}</h4>
                        <p><label>To: </label><a className="mail-to">{mailDetail.to}</a></p>
                        <p className="mail-body"><label>Body: </label><br></br>{mailDetail.body}</p>
                    </div>
                    :null
                    } 
                </Col>
            </Row>
            </div>
        )
    }
}

export default MyComponent