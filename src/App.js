import React from 'react';
import './App.css';
import FolderList from "./component/folderList"


function App() {
  return (
    <div className="App">
      <FolderList />
    </div>
  );
}

export default App;
